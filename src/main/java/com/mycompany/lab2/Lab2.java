/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {

    static char currentPlayer = 'X';
    static char num;
    static char[][] table = {{'7', '8', '9'}, {'4', '5', '6'}, {'1', '2', '3'}};
    static char yesNo = 'y';

    static void printWelcome() {
        System.out.println("Welcome OX");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + table[i][j]);
            }
            System.out.println();
        }
    }

    static void printTurn() {
        System.out.println(currentPlayer + " Turn");

    }

    static void inputNum() {
        Scanner kb = new Scanner(System.in);
        System.out.print("please input number: ");
        num = kb.next().charAt(0);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (num == table[i][j]) {
                    table[i][j] = currentPlayer;
                }
            }
        }
    }

    static void changeTurn() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer && table[i][1] == currentPlayer && table[i][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == currentPlayer && table[1][i] == currentPlayer && table[2][i] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    static boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][table.length - 1 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean isWin() {
        if (checkRow()||checkCol()||checkX1()||checkX2()) {
            return true;
        }
        return false;
    }

    static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;
    }

    static char intputYesNo(char n) {
        Scanner kb = new Scanner(System.in);
        System.out.print("do you want to continue (y or n) : ");
        return yesNo = kb.next().charAt(0);
    }

    static void reGame() {
        table = new char[][]{{'7', '8', '9'}, {'4', '5', '6'}, {'1', '2', '3'}};
        currentPlayer = 'X';
    }

    static void showWin() {
        System.out.println(currentPlayer + " Win!!!");
    }

    public static void main(String[] args) {
        printWelcome();
        while (yesNo == 'y') {
            while (true) {
                printTable();
                printTurn();
                inputNum();
                if (isWin()) {
                    printTable();
                    showWin();
                    break;
                }
                if (isDraw()) {
                    printTable();
                    System.out.println("Draw");
                    break;
                }
                changeTurn();
            }
            if (intputYesNo(yesNo) != 'y') {
                break;
            }
            reGame();
        }
    }
}
